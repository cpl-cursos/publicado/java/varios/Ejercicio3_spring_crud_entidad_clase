package com.cpl_cursos.java.ejemplos.crud_simple.dao;

import com.cpl_cursos.java.ejemplos.crud_simple.entity.Provincia;
import com.cpl_cursos.java.ejemplos.crud_simple.entity.Usuario;

import java.util.List;

public interface IfxProvincia {
    public List<Provincia> findAll();
    public Provincia findOne(Long id);
    public void save(Usuario provincia);
    public void delete(Long id);
}

package com.cpl_cursos.java.ejemplos.crud_simple.controllers;

import com.cpl_cursos.java.ejemplos.crud_simple.dao.IfxCategoria;
import com.cpl_cursos.java.ejemplos.crud_simple.entity.Categoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Controller
@SessionAttributes("categoria") //<-- es el nombre que se pasa a las vistas y que aparece también en el formulario
public class CategoriaController {

    @Autowired
    private IfxCategoria categoriaDao;

    @GetMapping("/listacategorias")
    public String listacat(Model modelo) {
        modelo.addAttribute("titulopest", "Categorías");
        modelo.addAttribute("titulo", "Listado de categorías");
        List<Categoria> lista = categoriaDao.findAll();
        modelo.addAttribute("categorias", lista);
        return "lista_categorias";
    }

    @GetMapping("/categoria/{id}")
    public String editar(@PathVariable(name="id") Long idcat, Map<String, Object> modelo) {
        modelo.put("titulopest", "Perfil");
        modelo.put("titulo", "Perfil de usuario");
        Categoria cat;
        if(idcat > 0) {
            cat = categoriaDao.findOne(idcat);
        } else {
            return "redirect:/listacategorias";
        }
        modelo.put("categoria", cat);
        return "form_categoria";
    }

    @GetMapping("/nuevacategoria")
    public String nuevo(Model modelo) {
        modelo.addAttribute("titulopest", "Nueva Categoría");
        modelo.addAttribute("titulo", "Añadir categoría");
        Categoria cat = new Categoria();
        modelo.addAttribute("categoria", cat);
        return "form_categoria";
    }

    @PostMapping("/nuevacategoria")
    /*
        @Valid permite verificar los datos recibidos partiendo de las restricciones impuestas en la clase Usuario.
        BindinResult asigna los resultados de la validación a la variable resultado. Contiene los errores. DEBE ponerse
        DESPUES del objeto a validar
     */
    public String nuevo(@Valid Categoria categoria, BindingResult resultado, ModelMap modelo, SessionStatus estado) throws ParseException {
        /*
            Como hemos utilizado @InitBinder, ya no es necesario utilizarlo aquí.
         */
        // validamos el formulario
        //validador.validate(usuario, resultado);

        // forma manual de obtener los errores
        if (resultado.hasErrors()) {
            /*
                Puedo haber llegado aquí desde la ruta con un id de usuario o desde la ruta de nuevo usuario.
                Tengo que volver al mismo sitio en caso de error en el formulario.
             */
            if(categoria.getIdCategoria() != null && categoria.getIdCategoria() > 0){
                return "redirect:/categoria/".concat(categoria.getIdCategoria().toString());
            } else {
                return "redirect:/nuevacategoria";
            }
        }

        modelo.addAttribute("titulopest", "Categoría");
        modelo.addAttribute("titulo", "Categoría");
        modelo.addAttribute("usuario",categoria);
        categoriaDao.save(categoria);
        estado.setComplete();
        return "redirect:/listacategorias";
    }
}

package com.cpl_cursos.java.ejemplos.crud_simple.validadores;

import com.cpl_cursos.java.ejemplos.crud_simple.entity.Usuario;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component //<-- para que se pueda inyecta en el controlador
public class UsuarioValidador implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Usuario.class.isAssignableFrom(clazz); //<-- Para asegurar que el objeto que se está validando es de la clase Usuario
    }

    @Override
    public void validate(Object target, Errors errors) {
        Usuario usuario = (Usuario) target; //<-- aseguramos el objeto evaluado como un Usuario
        // El tercer argumento de la siguiente línea es el nombre definido para el error en messages.properties
        ValidationUtils.rejectIfEmpty(errors, "nombre", "NotEmpty.usuario.nombre");
        ValidationUtils.rejectIfEmpty(errors, "apellidos", "NotEmpty.usuario.apellidos");
        ValidationUtils.rejectIfEmpty(errors, "email", "NotEmpty.usuario.email");
        ValidationUtils.rejectIfEmpty(errors, "creadoEl", "NotEmpty.usuario.creadoEl");

        /* Otra opción de hacer lo mismo
        if(usuario.getNombre().isEmpty()) {
            errors.rejectValue("nombre", "NotEmpty.usuario.nombre");
        }
        */
    }
}

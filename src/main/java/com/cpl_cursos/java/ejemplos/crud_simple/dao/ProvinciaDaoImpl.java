package com.cpl_cursos.java.ejemplos.crud_simple.dao;

import com.cpl_cursos.java.ejemplos.crud_simple.entity.Provincia;
import com.cpl_cursos.java.ejemplos.crud_simple.entity.Usuario;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("ProvinciaDaoJPA")  //<-- indica que es un elemento de Spring
public class ProvinciaDaoImpl implements IfxProvincia{

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Provincia> findAll() {
        return em.createQuery("from Provincia").getResultList();
    }

    @Override
    public Provincia findOne(Long id) {
        return null;
    }

    @Override
    public void save(Usuario provincia) {

    }

    @Override
    public void delete(Long id) {

    }
}

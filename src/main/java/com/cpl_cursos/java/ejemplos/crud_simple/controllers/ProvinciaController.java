package com.cpl_cursos.java.ejemplos.crud_simple.controllers;

import com.cpl_cursos.java.ejemplos.crud_simple.dao.IfxProvincia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProvinciaController {

    @Autowired
    private IfxProvincia provDao;

    @GetMapping("/listaprovincias")
    public String listaProv (Model modelo){
        modelo.addAttribute("titulo","Lista de Provincias");
        modelo.addAttribute("titulopest", "Provincias");
        modelo.addAttribute("provincias", provDao.findAll());
        return "lista_provincias";
    }
}

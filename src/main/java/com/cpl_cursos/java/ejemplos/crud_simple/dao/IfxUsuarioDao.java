package com.cpl_cursos.java.ejemplos.crud_simple.dao;

import com.cpl_cursos.java.ejemplos.crud_simple.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

// No hace falta anotarlo com componente Spring porque CrudRepository ya lo es
public interface IfxUsuarioDao extends CrudRepository<Usuario, Long> {

}

package com.cpl_cursos.java.ejemplos.crud_simple.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="provincias")
public class Provincia implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idprovincia", nullable = false)
    private Long idProvincia;
    @Column(nullable = false, unique = true)
    private String provincia;
    @Column(name = "codprovincia", nullable = false, unique = true)
    private String codProvincia;
    private static final Long serialVersionUID = 1L;

    public Provincia() {
    }

    public Provincia(Long idProvincia, String provincia, String codProvincia) {
        this.idProvincia = idProvincia;
        this.provincia = provincia;
        this.codProvincia = codProvincia;
    }

    public Long getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(Long idProvincia) {
        this.idProvincia = idProvincia;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCodProvincia() {
        return codProvincia;
    }

    public void setCodProvincia(String codProvincia) {
        this.codProvincia = codProvincia;
    }
}
